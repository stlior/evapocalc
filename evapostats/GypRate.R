RainArr = seq(0,50,by = 1);
DustArr = c(10);
repetition = 1:1;
PETseq = (2100)

rainDustArray = tibble(crossing(RainArr, DustArr, PETseq, repetition));

annual = seq(270, 270, by = 10);
days = c(10,15,20,25,30);
array = tibble(crossing(days, annual), mean = annual / days) %>% filter(mean >= 3 & mean <= 11)
array = tibble(annual, days = annual / 8)

resRate = list()
for (i in 1:nrow(array)) {
    wantedDays = array$days[i];
    wantedRain = array$annual[i];
    print(paste("iteration: ", i))
    SynthRainEP = GenerateSeries(station = 347700, stationEvap = 347704, NumOfSeries = 30, AnuualRain = array$annual[i], WetDays = array$days[i])


    print("sendind to calculation: 1")
    shehoret = lapply( 1:nrow(rainDustArray),  function(X) CalcGypsum(SynthRainEP, duration = 1000, Depth = 100, rainSO4 = rainDustArray$RainArr[X], dustFlux = rainDustArray$DustArr[X], PETFactor = getPETfactor(347700, rainDustArray$PETseq[X]), WRain = wantedRain, Wdays = wantedDays, Wpet = rainDustArray$PETseq[X], rainstat = T, withRunoff = F, withFC = F))

    resRate = c(resRate, shehoret)
    # results$talus1 = c(results$talus1, parLapply(cl, 1:nrow(rainDustArray), fun = function(X) CalcGypsum(SynthRainS, SynthRainSP, duration = Talus1Observed$AvgAge[1], Depth = 150, rainSO4 = rainDustArray[X, 1], dustFlux = rainDustArray[X, 2], AETFactor = rainDustArray[X, 3])))


}
save(resRate, file = "resRate.RData")

resRateTable = resRate %>% RectanglingResultsseq(T1.1) %>% bind_cols(resRate %>% map_dfr(.f = ~(tibble(sumSulfate = sum(.x$YearSulfate)))))
#convert sum sulfate to mg/year  amd dustflux to mg/yr
resRateTable = resRateTable %>% mutate(sumSulfate = sumSulfate * 96.06 / 1000*1000, sumDustFlux = dustFlux / 10000*0.005*1000)
#resRateTable = resRate %>% map_df(.f = ~tibble(rain = .x$rainStat$annual, RainDays = .x$rainStat$n, DFlux = .x$DF * .x$DGyp, RSO4 = .x$RSO4, gyp = .x$YearGyp, Rainsulfate = .x$YearSulfate * 96.06 * 1000, gypsub = gyp - lag(gyp), totSulfate = Rainsulfate + DFlux)) %>% drop_na() %>% filter(rain < 100 & rain > 0)
res = resRateTable %>% filter()
AnnualRain/rainDays
ggplot(res, aes(sumSulfate, AnnualRain, color = total)) + geom_point(size = 4, alpha = 1) + scale_color_gradientn(colours = rainbow(5, rev = T)) + labs(x = "sulfate [mg/yr]", y = "yearly rain [mm]", color = "Gypsum \naccumulation rate \n [meq/100gr soil /yr]") + coord_cartesian()
#interpulate
bla = interp(res$AnnualRain, res$sumSulfate , res$total / 1000, duplicate = "median", ny = 100, nx = 100)
d2 <- as_tibble(melt(bla$z, na.rm = T))
names(d2) <- c("rain", "totSulfate", "gypsub")

d2$rain <- bla$x[d2$rain]
d2$totSulfate <- bla$y[d2$totSulfate]
ggplot(d2, aes(totSulfate, rain, fill = gypsub, z = gypsub, color = gypsub)) + geom_raster(interpolate = F) + geom_contour(color = "black", breaks = seq(0.0005, 0.014, by = 0.0005)) + scale_fill_gradientn(colours = rainbow(5, rev = T), breaks = seq(0.001, 0.016, by = 0.002)) + labs(x = "Annual sulfate influx [mg/yr]", y = "Annual rain [mm]", fill = "Gypsum \naccumulation rate \n [meq/100gr soil /yr]") + coord_cartesian(ylim = c(15, 430)) + scale_fill_distiller(palette = "Oranges", direction = 1) + geom_text_contour(color = "black", skip = 0.0005, stroke = 0.2,min.size = 50) + scale_x_continuous(expand = c(0, 0)) + scale_y_continuous(expand = c(0, 0),breaks = seq(10,440,by = 20))
#sample data

GenerateSeries(station = 347700, stationEvap = 347704, NumOfSeries = 30, AnuualRain = 250, WetDays = 250 / 8) %>% group_by(year) %>% summarise(sum(rain)) %>% summarise_all(mean)
